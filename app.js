const express = require('express');
const mongodb = require('mongodb');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
var cors = require('cors');

dotenv.config();

const app = express();
const port = 3001;
const client = new mongodb.MongoClient(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(bodyParser.json());
app.use(cors());

/**
 * GET /product/{id}
 * Returns the product with the given id
 */
app.get('/product/:id', async (req, res) => {
    try {
        await client.connect();
        const db = client.db();
        const product = await db.collection('products').findOne({
            _id: new mongodb.ObjectId(req.params.id)
        });

        if (product) {
            res.status(200).send(product);
            return;
        } else {
            res.status(404).send(`Product with id ${req.params.id} not found`);
            return;
        }

    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');

    } finally {
        await client.close();
    }
});

/**
 * POST /product
 * Create a new product
 */
app.post('/product', async (req, res) => {
    try {
        const product = req.body;
        const { name, isDLC, date } = product;
        if (name === undefined || isDLC === undefined || date === undefined) {
            res.status(400).send('Missing data for the product creation');
            return;
        }

        await client.connect();
        const db = client.db();

        const result = await db.collection('products').insertOne({ name, isDLC, date });
        res.status(201).send({ id: result.insertedId });
        return;

    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');

    } finally {
        await client.close();
    }
});


/**
 * POST /receipt
 * Create a new receipt
 */
app.post('/receipt', async (req, res) => {
    try {
        const { idList } = req.body;
        if (idList === undefined || idList.length === 0) {
            res.status(400).send('You must add one or more products');
            return;
        }

        await client.connect();
        const db = client.db();

        const result = await db.collection('receipts').insertOne({ idList });
        res.status(201).send({ id: result.insertedId });
        return;

    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');

    } finally {
        await client.close();
    }
});

/**
 * GET /receipt
 * Get products of the receipt with the given id
 */
app.get('/receipt/:id', async (req, res) => {
    try {
        await client.connect();
        const db = client.db();

        const receipt = await db.collection('receipts').findOne({
            _id: new mongodb.ObjectId(req.params.id)
        });

        if (!receipt) {
            res.status(404).send(`Receipt with id ${req.params.id} not found`);
            return;
        }

        const productIds = receipt.idList.map(id => new mongodb.ObjectId(id));

        const products = await db.collection('products').find({
            _id: { $in: productIds }
        }).toArray();
        res.status(200).send(products);
        return;

    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');

    } finally {
        await client.close();
    }
});


/**
 * Start listening on port 3000
 */
app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
});
