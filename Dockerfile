FROM node:20-alpine3.17

WORKDIR /app

COPY package*.json ./

RUN npm install --production

COPY . .

EXPOSE 3001

CMD ["npm", "start"]

# --- BUILD AND RUN ---
# docker build -t expressjs-asphalt-product .
# docker run -p 3001:3001 --name asphalt_service_product -d expressjs-asphalt-product

# --- DELETE IMAGE ---
# docker image rm expressjs-asphalt-product